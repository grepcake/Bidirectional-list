#include <iostream>
#include "List.h"
#include <gtest/gtest.h>
#include "tests.inl"

int main(int ac, char **av) {
    testing::InitGoogleTest(&ac, av);
    return RUN_ALL_TESTS();
}
